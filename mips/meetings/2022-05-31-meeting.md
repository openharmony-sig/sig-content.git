# May.31, 2022 at 10:00am GMT+8

## Agenda
|时间|议题|发言人|
|--|--|--|
|10:00-10:15|Mips第一阶段任务完成情况梳理，第二阶段进展同步|袁祥仁 李兵|
|10:15-10:25|Mips系统编译适配进展同步|Lain 李兵 黄慧进 刘佳科|
|10:25-10:35|Mips适配中y2038(32位时间2038年溢出)问题情况交流|Lain 李兵 黄首西|

## Attendees
- [@hongtao6573](https://gitee.com/hongtao6573)
- [@cip_syq](yunqiang.su@oss.cipunited.com)
- [@Lain]()
- [@wicom](https://gitee.com/wicom)
- [@wangxing-hw](https://gitee.com/wangxing-hw)
- [@libing23](https://gitee.com/libing23)
- [@huangsox](https://gitee.com/huangsox)
- [@liujk000](https://gitee.com/liujk000)
- [@huanghuijin]()

## Notes

#### 议题一、Mips第一阶段任务完成情况梳理，第二阶段进展同步

**结论**
- 子系统适配按计划正常推进中，一阶段pr已提交，当前适配中的模块为display和camera。


#### 议题二、Mips系统编译适配进展同步

**结论**
- 编译框架适配预计7月初初步跑起来。
- 三方系统组件(系统三方库)适配，计划先适配openssl库作为示例。
- 编译工具链目前使用linux命令安装的版本（clang11），OH版本clang/llvm编译工具链计划在6月底开源，之后可在其基础上适配mips平台，最终目标是同一个编译工具链根据配置可同时支持arm和mips。


#### 议题三、Mips适配中y2038(32位时间2038年溢出)问题情况交流

**结论**
- y2038问题当前不对其他任务造成阻塞，优先级较低，后续继续关注。

## Action items
