
## Agenda

- OH 辅助工具 SIG例会

  |    时间     | 议题                                                     | 发言人                 |
  | :---------: | -------------------------------------------------------- | ---------------------- |
  | 16:00-16:30 | 议题一、NAPI框架代码生成工具一阶段需求毕业遗留问题对齐      | 赵军霞       |


## Attendees

- [@zhaojunxia2020](https://gitee.com/zhaojunxia2020)
- [@joeysun001](https://gitee.com/joeysun001)
- [@xudong-zhao](https://gitee.com/xudong-zhao)


## Notes

- 议题一、NAPI框架代码生成工具一阶段需求毕业遗留问题对齐
  一阶段需求准备330入主干仓 例会时间定于每周三16:00-17:00，会议当周[赵军霞](https://gitee.com/zhaojunxia2020)在共享文档中收集议题[申报议题](https://shimo.im/sheets/ppCXWxYr68k3JPk9/MODOC)。
  

  
  ## Action items

   1、清理门禁问题 --赵旭东
   2、补充自动化测试用例--陈迅
   3、整改文档目录--孙仲毅
   4、修改仓名、对接IDE --赵军霞


