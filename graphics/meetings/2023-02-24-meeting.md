# February 6, 2023 at 17:00pm GMT+8

## Agenda
- 申请建仓third_party_Vulkan-Loader
- 申请建仓third_party_Vulkan-ValidationLayers

## Attendees
- @abbuu(https://gitee.com/abbuu)
- @woniugb(https://gitee.com/woniugb)
- @andrew0229(https://gitee.com/andrew0229)
- @xxfeng-hw(https://gitee.com/xxfeng-hw)

## Notes

会议纪要：

- 同意引入Vulkan-Loader三方软件，建立sig仓
- 同意引入Vulkan-ValidationLayers三方软件，建立sig仓

## Action items
- NA
