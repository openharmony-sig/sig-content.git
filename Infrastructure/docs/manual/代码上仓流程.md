# 概述

该教程旨在引导开发者了解如何将代码放到社区。


代码上仓流程：

1. 确认在社区下是否已经存在对应的仓库，如果没有，可参照[更新社区sig信息指导](./更新社区sig信息.md)提建仓申请
2. 最重要：开发过程中一定要记得使用git commit -s -m添加signed-off信息，且signed-off中涉及到的邮箱已经在[链接](https://dco.openharmony.io/sign/Z2l0ZWUlMkZvcGVuX2hhcm1vbnk=)中签署DCO  **且已[签署DCO](https://dco.openharmony.io/sign/Z2l0ZWUlMkZvcGVuX2hhcm1vbnk=)
3. 每个仓需要提供仓描述文件read.md ，其中包括编译指导说明，参考[仓描述文档模板](./仓描述文档模板.md)
4. 保证上仓的代码已添加相应的版权信息（开源许可证、版权头等信息）
5. 向sig下的[manifest仓](https://gitee.com/openharmony-sig/manifest)新增对应开发板的xml文件

  * 提交以后，后台会进行自动化的法务合规、cicd的扫描，扫描结果会在提交的PR下以评论的形式展现出来
  * 如果扫描有问题，需要根据反馈进行整改，然后再重新提交，直到审核通过

