#  Mar 25, 2022 at 15:00pm-16:30pm GMT+8

## Agenda

- OH Dev-Board SIG例会

- | 时间        | 议题                             | 发言人 |
  | ----------- | -------------------------------- | ------ |
  | 15:00-15:10 | 各单位开发板状态更新             | 赵秀秀 |
  | 15:10-15:20 | devboard开发板上官网事项汇报     | 李凯   |
  | 15:20-15:30 | 瘦设备硬件接口规范编写框架与分工 | 连志安 |
  | 15:30-15:40 | 知识体系工作组工作进展交流       | 张前福 |
  | 15:40-15:50 | 425技术日开发板展示情况介绍      | 林丹妮 |
  | 15:50-16:00 | 开发讨论                         | 全体   |

- [@liuyang198591](liu_yang@hoperun.com)

- [@Laowang-BearPi](wangcheng@holdiot.com)

- [@zhao_xiuxiu ](1605427044@qq.com)

- [@kevenNO1 ](likai20@iscas.ac.cn)

- [@jony_code ](longjun@iscas.ac.cn)

- [@minglonghuang ](minglong@iscas.ac.cn)

## Notes

- 议题一、各单位开发板状态更新
  按照适配中、符合SIG准出标准、兼容性认证通过三种状态将目前 OH Dev-Board SIG 看护的开发板介绍，当前开发板进展详情请阅【腾讯文档】DevBoard-SIG开发板列表
  https://docs.qq.com/sheet/DYmZ1RmhEZ1RVa0to
  
- 议题二、 devboard开发板上官网事项汇报  

  以下是开发板上官网的一个初稿，本初稿自2022年4月22日开放一周
  1. 【腾讯文档】官网关于SIG展示页的设计提议V1.2
  https://docs.qq.com/slide/DUVdWRk9YRE9TcG1Z

  2. 【腾讯文档】sig devboard 开发板上官网信息收集表V1.1
  https://docs.qq.com/sheet/DUUtGb3BvRVBCZWRT

- 议题三： 瘦设备硬件接口规范编写框架与分工
  
  瘦设备硬件接口规范希望各个成员单位都能参与编写，目前写了整体框架，大家都可以认领一下自己编写的规范，希望每个成员单位都能参与，当前编写进度需要加急，工作组计划一周后完成认领。【腾讯文档】OpenHarmony瘦设备规范编写
  https://docs.qq.com/doc/DRW5Ud2FFeEtOWWl4
  
- 议题四：知识体系工作组工作进展交流

  知识体系工作组接口人：张前福，相关交流文档【腾讯文档】知识体系进展share-20220422
https://docs.qq.com/pdf/DRmxReG90T2RtY2RW
  
- 议题五：425技术日开发板展示情况介绍

  425展示开发板的条件是要求开发板已经通过兼容性认证的开发板。受疫情影响，线下参展人数有限，为了活动效果，欢迎大家转发线上直播链接

  

  ## Action items

  1、加快瘦设备硬件接口规范的制定和完善。---连志安&赵秀秀

  

  

  

  