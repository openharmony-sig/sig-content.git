#  March 2, 2022 at 16:00am-18:00am GMT+8

## Agenda

- OH Driver SIG例会[](https://meeting.tencent.com/s/O6GmTrcZcV8a)

  |    时间     | 议题                                 | 发言人                                             |
  | :---------: | ------------------------------------ | ------------------------------------------------- |
  | 16:00-16:10 | 议题一、Media的codec驱动能力开发进展 | 张国荣     |
  | 16:10-16:30 | 议题二、启动子系统模块新增HDI接口评审 | 张小田，李浩，翟海鹏，袁博，刘飞虎 |
  | 16:30-16:50 | 议题三、Codec新增2.0 HDI接口评审 | 孙赫赫，张国荣，翟海鹏，袁博，刘飞虎 |
## Attendees

- @crescenthe(https://gitee.com/crescenthe)
- @vb6174(https://gitee.com/vb6174) 
- @zhangguorong(zhangguorong050@chinasoftinc.com)
- @YUJIA(https://gitee.com/JasonYuJia)
- @yuanbo(https://gitee.com/yuanbogit)
- @Kevin-Lau(https://gitee.com/Kevin-Lau)

  

## Notes

- **议题一、 Media的codec驱动能力开发进展**

    议题进展：Codec2.0接口已定义，接口待评审后提交。基于2.0接口，对接omx的demo已开发验证，待整理提交；基于1.0接口 对接mpp的demo在开发验证，解码已完成，编码完成50%。 

- **议题二、 启动子系统模块新增HDI接口评审**

   议题结论：新增启动子系统deviceToken模块HDI接口定义，接口和结构体设计合理，接口评审通过。

   新增IDL接口如下：

   ReadToken([out] string token);

   WriteToken([in] string token);

   GetAcKey([out] string acKey);

   GetProdId([out] string productId);

   GetProdKey([out] string productKey);

- **议题三、 Codec新增2.0 HDI接口评审**

   议题结论：新增codec 2.0版本HDI接口定义，部分接口和结构体待优化，待遗留问题闭环重新评审。

遗留问题：1.结构体和函数参数优化；2.部分结构体定义合规性确认

新增接口定义：https://gitee.com/openharmony/drivers_peripheral/pulls/737/files 

   

   会议通知：https://lists.openatom.io/hyperkitty/list/sig_driver@openharmony.io/thread/LNB3GJCT7HYSTGQ2V3462UXPEACGML4G/

   会议议题申报：https://etherpad.openharmony.cn/p/SIG-Driver

   会议归档：https://gitee.com/openharmony-sig/sig-content/tree/master/driver/meetings


  ## Action items
