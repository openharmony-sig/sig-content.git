#  November 10, 2021 at 16:00am-18:00am GMT+8

## Agenda

- OH Driver SIG例会[](https://meeting.tencent.com/s/O6GmTrcZcV8a)

  |    时间     | 议题                                           | 发言人                                                       |
  | :---------: | ---------------------------------------------- | ------------------------------------------------------------ |
  | 16:00-16:15 | 议题一、Media的codec驱动能力开发进展           | crescenthe，vb6174，starfish，magekkkk，zhangguorong，YUJIA  |
  | 16:10-16:40 | 议题二、电源模块HDI接口评审                    | zhuangchunxun，Kevin-Lau，NickYang，YUJIA，yuanbo            |
  | 16:40-17:00 | 议题三、ppg接入OpenHarmony进展、问题及后续计划 | 郭超胜，Muffin ，武和波，muffin，YUJIA，Kevin-Lau，NickYang， |

## Attendees

- @crescenthe(https://gitee.com/crescenthe)
- @郭超胜
- @Muffin 
- @武和波(https://gitee.com/371931794)
- @muffin(https://gitee.com/371931794)
- @NickYang(https://gitee.com/haizhouyang)
- @Kevin-Lau(https://gitee.com/Kevin-Lau)
- @vb6174(https://gitee.com/vb6174)
- @magekkkk(https://gitee.com/magekkkk)
- @yuanbo(https://gitee.com/yuanbogit）
- @YUJIA(https://gitee.com/JasonYuJia)
- @zhangguorong(zhangguorong050@chinasoftinc.com)
- @starfish(https://gitee.com/starfish002) 

## Notes

- **议题一、Media的codec驱动能力开发进展 **

   结论：1、当前codec选择模块，buffer管理模块，capability配置模块已完成开发，代码review和本地验证完成后，评审代码准入。 2、OMX的方案文档已完成90%的设计，11月14日前完成方案文档。下一步按照codec计划完成剩余模块开发，交付时间为11月30日。
   
- **议题二、电源模块HDI接口评审**

   结论：电源子系统新增电源热控模块5个接口，接口和数据设计合理，接口评审通过。

   贡献的代码完成功能自验证后，提交master分支PR，发起评审和review。 

- **议题三、ppg接入OpenHarmony进展、问题及后续计划 **

   结论：1、会议对当前sensor框架和Sensor驱动模块进行定制化修改方案讨论，解决调测ppg过程中问题，在OpenHarmony应用层可以接收到ppg数据，并绘图ppg曲线图。 

   2、根据当前sensor服务框架和驱动模型，介绍ppg的服务框架和驱动模型设计方案，方案包括北向接口，数据通道，南向接口。方案需要细化讨论，会后针对南向接口方案，北向接口方案，数据通道方案单独拉会进行讨论。 

   

   会议通知：https://lists.openatom.io/hyperkitty/list/sig_driver@openharmony.io/thread/77QYTL66WGGLH3NH3JMEZQHIY4Z7LMQA/

   会议议题申报：https://docs.qq.com/sheet/DZnpsUFZIT3BVcWJ5?tab=BB08J2&_t=1632723145103 

   会议归档：https://gitee.com/openharmony-sig/sig-content/tree/master/driver/meetings


  ## Action items

  

