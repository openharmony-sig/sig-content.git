# OpenHarmony社区Openchain认证-末次会议2023-6-26 11:00-11:30(UTC+08:00)Beijing

## 议题Agenda

议题1、OpenHarmony社区Openchain认证-末次会议 于昕


## 与会人Attendees
- 于昕，高亮，朱伟，王潮，施华南，陈一雄，周乃鑫


## 纪要Notes
本次会议主要内容：

议题1、OpenHarmony社区OpenChain认证结果反馈
1、中国电子技术标准化研究院 于昕老师，宣布了OH社区OpenChain认证通过。

下一步计划
1、高亮老师 将Openchain认证通过结果上报OH社区工委会，并推动认证通过结果的宣传推广 ；
2、后续纸质和电子认证证书会发送给OH社区 ；
3、于昕老师 将协助确认OH社区的认证结果如何在Openchain官网上公示及Openchain官方渠道宣传事宜。
