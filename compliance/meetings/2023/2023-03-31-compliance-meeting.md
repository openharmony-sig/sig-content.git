# 合规SIG例会 2023-3-31 15:30-16:30(UTC+08:00)Beijing

## 议题Agenda
1、新成员加入（欢迎）高雅

2、合规审查能力-许可证合规审查机制目标讨论 高亮、陈雅旬、丛林

3、对外贡献合规-开源贡献Policy目标讨论 高亮

4、对外贡献合规-管理开源贡献流程文档目标讨论 高亮 

5、合规制品生产和交付-开源义务履行目标讨论 5min 陈雅旬

6、合规制品生产和交付-合规制品归档及生命周期目标讨论 陈雅旬

7、《开源合规策略及指导》评审 方晓

8、《合规SIG运作治理章程》评审 高亮

## 与会人Attendees
- 高亮、余甜、陈一雄、陈雅旬、施华南、赵鹏、关仁杰、高雅、赵秀成、方晓、周乃鑫、高琨

## 纪要Notes
议题1、新成员加入
会议结论
欢迎来自美的的高雅加入合规SIG

议题2、合规审查能力-许可证合规审查机制目标讨论
经SIG组成员讨论，本子专项23年主要目标：

1、输出《二进制管理规则》

2、OAT增强二进制门禁检查能力

3、Fosscan 2.0 升级增强许可证篡改检查

4、配套“开源许可证识别分析”专项-1 中兼容性规则

5、输出《图片等资源文件管理规则》

6、北向：TPC下开源合规管理规则及工程能力提升

7、南向：下游厂商开源合规工程工具能力复用

议题3、对外贡献合规-开源贡献Policy目标讨论
经SIG组成员讨论，本子专项23年主要目标：

1、输出《上游社区贡献开源合规指导》，已提交PR，请SIG成员检视，后续刷新定稿后进行评审。

https://gitee.com/openharmony/docs/blob/48c9aea1def80a0a11e098a68fb4a881158…

议题4、对外贡献合规-管理开源贡献流程文档目标讨论
经SIG组成员讨论，本子专项23年主要目标：

1、《上游开源软件贡献管理流程》及在官方网址上体现对应的策略， 上游贡献统计，本子专项优先级放低。

议题5、合规制品生产和交付-开源义务履行目标讨论
经SIG组成员讨论，本子专项23年主要目标：

1、输出《义务履行交付制品说明》

议题6、合规制品生产和交付-合规制品归档及生命周期目标讨论
经SIG组成员讨论，本子专项23年主要目标：

1、        输出《合规制品管理流程》

议题7、《开源合规策略及指导》评审
会议结论：

1、SIG组内评审通过《开源合规策略及指导》，下一步去法务合规组及PMC评审
https://gitee.com/openharmony/docs/blob/48c9aea1def80a0a11e098a68fb4a881158…

议题8、《合规SIG运作治理章程》评审
会议结论：

1、        SIG组内评审暂不通过《合规SIG运作治理章程》，涉及SIG选举内容，需和PMC保持同步，待与PMC汇报后刷新。
https://gitee.com/openharmony/docs/blob/48c9aea1def80a0a11e098a68fb4a881158…

议题9、 其他

1、陈一雄，高琨补充业界活动信息OpenEuler Developer Day 4月21日上海 
https://shimo.im/forms/ajofLUZ8Wac7lOJY/fill

2、赵鹏补充业界活动信息OpenHarmony Developer Day 4月20日 北京

3、赵鹏提出当前社区的开源合规工具，哪些是否可以由合作伙伴可使用和复用的，希望在下一次例会进行讨论。



